# Roulette
In this project, I wanted to practice ReactJS and other technologies as CI with GitLab and Unit testing

## How used it?
Push button "add", it opens the modal where you put option name and the add it, when you finished adding all option you wanted, push "start" and after 2 seconds it does open modal with the winner option.

## Commands
    yarn install
    yarn start  // or yarn build

## Technologies
- React
- TailwindCSS
- Jest
- Gitlab CI
