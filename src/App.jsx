import React, {useState} from 'react'
import ListGuns from './components/ListGuns'
import Modal from 'components/Modal'
import ModalWinner from 'components/ModalWinner'

const App = () => {
  const [selected, setSelected] = useState(-1)
  const [loading, setLoading] = useState(false)
  const [items, setItems] = useState([])
  const [winner, setWinner] = useState('')
  const [showWinner, setShowWinner] = useState(false)
  const [show, setShow] = useState(false)


  const handleWinner = () => {
    setShowWinner(true)
  }

  const startGame = () => {
    setShowWinner(false)
    if (!loading) {
      setLoading(true)
      const intervalTimer = setInterval(() => {
        const value = Math.floor((Math.random() * (items.length)))
        setSelected(value)
        if (value > -1 && value < items.length) {
          setWinner(items[value].title)
        }
      }, 100);

      setTimeout(() => {
        clearInterval(intervalTimer)
        setLoading(false)
        handleWinner()
      }, 2000)
    }
  }


  const handleAdd = (value) => {
    setItems([...items, {
      title: value
    }])
    setShow(false)
  }

  const openModal = () => {
    setShow(true)
  }
  return (
    <div className="p-4">
      <header className="w-full text-2xl font-bold text-white uppercase select-none md:text-4xl">
        Roulette
      </header>
      <Modal onAccept={handleAdd} onClose={() => setShow(false)} show={show} />
      <ModalWinner winner={winner} show={showWinner} onClose={() => setShowWinner(false)} />
      <ListGuns selected={selected} items={items} />
      <div className="flex flex-wrap w-full py-4 md:flex-nowrap md:justify-between">
        <button
          onClick={openModal}
          disabled={loading}
          role='add-card'
          className="w-full bg-red-500 btn hover:bg-red-600 md:w-52">
          Add
        </button>
        <div className="flex w-full mt-2 md:justify-end md:mt-0">
          <button className="w-16 mr-4 bg-green-500 btn hover:bg-green-600 md:w-52">
            Credit
          </button>
          <button
            onClick={startGame}
            disabled={loading}
            className="w-full bg-indigo-500 btn hover:bg-indigo-600 md:w-52"
          >
            Roll
          </button>
        </div>
      </div>
    </div>
  )
}
export default App;
