import {array, number} from 'prop-types'
import React, {useEffect, useState} from 'react'
import Guns from './Guns'

const ListGuns = (props) => {
  const [items, setItems] = useState([])
  useEffect(() => {
    setItems(props.items || [])
  }, [props.items])

  return (
    <div className="container-cards">
      {items.map((x, i) => {
        return (
          <Guns key={i} value={`${i + 1}`} select={i === props.selected} />
        )
      })}
    </div>
  )
}

ListGuns.defaultProps = {
  items: [],
  selected: -1,
}

ListGuns.propTypes = {
  items: array.isRequired,
  selected: number
}

export default ListGuns
