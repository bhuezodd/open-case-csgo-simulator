import {bool, func, string} from 'prop-types'
import React from 'react'

const Modal = ({onClose, show, winner}) => {

  const handleClose = () => {
    onClose()
  }
  return (
    <div
      role="modal-winner"
      className={`fixed top-0 left-0 flex items-center justify-center w-full h-screen bg-black bg-opacity-50 ${show ? '' : 'hidden'}`}>
      <div className="absolute right-6 top-4">
        <svg
          onClick={handleClose}
          xmlns="http://www.w3.org/2000/svg"
          className="w-5 h-5 text-white cursor-pointer"
          viewBox="0 0 20 20"
          fill="currentColor">
          <path
            fillRule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clipRule="evenodd" />
        </svg>
      </div>
      <div className="w-full p-6 rounded-lg shadow md:w-2/3 lg:w-1/2 bg-primary-1">
        <header className="w-full text-2xl font-semibold text-white">
          {winner}
        </header>
      </div>
    </div>
  )
}

Modal.propTypes = {
  onClose: func,
  show: bool,
  winner: string
}

Modal.defaultProps = {
  onClose: () => false,
  show: true,
  winner: ''
}
export default Modal

