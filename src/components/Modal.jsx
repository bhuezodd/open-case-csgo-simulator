import {bool, func} from 'prop-types'
import React, {useState} from 'react'

const Modal = ({onClose, show, onAccept}) => {

  const [item, setItem] = useState('')

  const handleClose = () => {
    onClose()
  }
  const handleAccept = () => {
    onAccept(item)
    setItem('')
  }
  return (
    <div
      role="modal"
      className={`fixed top-0 left-0 flex items-center justify-center w-full h-screen bg-black bg-opacity-50 ${show ? '' : 'hidden'}`}>
      <div className="absolute right-6 top-4">
        <svg
          onClick={handleClose}
          xmlns="http://www.w3.org/2000/svg"
          className="w-5 h-5 text-white cursor-pointer"
          viewBox="0 0 20 20"
          fill="currentColor">
          <path
            fillRule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clipRule="evenodd" />
        </svg>
      </div>
      <div className="w-full p-6 rounded-lg shadow md:w-2/3 lg:w-1/2 bg-primary-1">
        <header className="w-full pb-2 mb-4 text-2xl font-semibold text-white border-b border-gray-500">
          Add to item
        </header>
        <label htmlFor="title" className="font-medium text-white">
          Title
        </label>
        <input autoFocus type="text" id="title" value={item} onInput={({target: {value}}) => setItem(value)} className="input" />

        <button onClick={handleAccept} className="w-full mt-5 bg-red-500 btn hover:bg-red-600">
          Add
        </button>
      </div>
    </div>
  )
}

Modal.propTypes = {
  onClose: func,
  onAccept: func,
  show: bool
}

Modal.defaultProps = {
  onClose: () => false,
  onAccept: () => false,
  show: true
}
export default Modal
