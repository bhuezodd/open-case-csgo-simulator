import {bool, string} from 'prop-types'
import React from 'react'

const Guns = (props) => {
  return (
    <div className="cover-card">
      <div className={`card ${props.select ? 'selected' : ''}`}>
        {props.value}
      </div>
    </div>
  )
}

Guns.defaultProps = {
  value: 0,
  select: false
}

Guns.propTypes = {
  value: string,
  select: bool
}

export default Guns
