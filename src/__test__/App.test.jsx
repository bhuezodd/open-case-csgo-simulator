import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import {fireEvent, render, screen} from '@testing-library/react';
import App from 'App'

describe('Create card', () => {
  let component = null
  beforeEach(() => {
    render(<App />)
    component = screen.getByRole('modal')
  })
  test('Modal closing', () => {
    expect(component).toHaveClass('hidden')
  })

  test('Open modal', () => {
    const button = screen.getByRole('add-card')
    fireEvent.click(button)
    expect(component).not.toHaveClass('hidden')
  })
})
