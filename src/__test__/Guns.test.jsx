import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import {render} from '@testing-library/react'
import Guns from 'components/Guns'

describe('Test card', () => {

  test('Card not selected', () => {
    const Gun = {
      value: '1',
      select: false
    }
    const component = render(<Guns {...Gun} />)

    const classes = component.getByText('1')
    expect(classes).not.toHaveClass('selected')
  })

  test('Card selected', () => {
    const Gun = {
      value: '2',
      select: true
    }
    const component = render(<Guns {...Gun} />)

    const classes = component.getByText('2')
    expect(classes).toHaveClass('selected')
  })

})
